USE [fachadas]
GO
/****** Object:  StoredProcedure [dbo].[Registrar_Usuario]    Script Date: 01/25/2019 16:24:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Registrar_Usuario]
@id_user int,
@nom_user varchar(35),
@pass_user varchar(45),
@nombre_user varchar(25),
@apellido_user varchar(25),
@email_user varchar (45)
AS
 BEGIN
    INSERT INTO dbo.Usuarios
            (id_user,nom_user,pass_user,nombre_user,apellido_user,email_user)
    VALUES  ( @id_user,
			  @nom_user 
             , ENCRYPTBYPASSPHRASE('password', @pass_user)  
             ,@nombre_user 
             ,@apellido_user
             ,@email_user  
             )
 end
