USE fachadas
-- -----------------------------------------------------
-- Table pais
-- -----------------------------------------------------
CREATE TABLE  pais (
  id_pais INT NOT NULL,
  nombre_pais VARCHAR(40) NULL,
  PRIMARY KEY (id_pais))



-- -----------------------------------------------------
-- Table Ciudad
-- -----------------------------------------------------
CREATE TABLE  Ciudad (
  id_ciudad INT NOT NULL,
  nombre_ciudad VARCHAR(40) NULL,
  pais_id_pais INT NOT NULL,
  PRIMARY KEY (id_ciudad),
  CONSTRAINT fk_ciudad_pais1
    FOREIGN KEY (pais_id_pais)
    REFERENCES pais (id_pais)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_ciudad_pais1_idx ON Ciudad (pais_id_pais ASC);


-- -----------------------------------------------------
-- Table localidad
-- -----------------------------------------------------
CREATE TABLE  localidad (
  id_localidad INT NOT NULL,
  nombre_localidad VARCHAR(40) NULL,
  localidadcol VARCHAR(45) NULL,
  ciudad_id_ciudad INT NOT NULL,
  PRIMARY KEY (id_localidad),
  CONSTRAINT fk_localidad_ciudad1
    FOREIGN KEY (ciudad_id_ciudad)
    REFERENCES Ciudad (id_ciudad)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_localidad_ciudad1_idx ON localidad (ciudad_id_ciudad ASC);


-- -----------------------------------------------------
-- Table Datos_profesionales
-- -----------------------------------------------------
CREATE TABLE  Datos_profesionales (
  id_datosprofesionales INT NOT NULL,
  Nivel_estudio VARCHAR(35) NULL,
  Profesion VARCHAR(40) NULL,
  PRIMARY KEY (id_datosprofesionales))



-- -----------------------------------------------------
-- Table Usuarios
-- -----------------------------------------------------
CREATE TABLE  Usuarios (
  id_user INT NOT NULL,
  nom_user VARCHAR(35) NULL,
  pass_user VARCHAR(45) NULL,
  nombre_user VARCHAR(25) NULL,
  apellido_user VARCHAR(25) NULL,
  email_user VARCHAR(45) NULL,
  PRIMARY KEY (id_user))



-- -----------------------------------------------------
-- Table horarios
-- -----------------------------------------------------
CREATE TABLE  horarios (
  id_horarios INT NOT NULL,
  hora_entrada DATE NULL,
  hora_salida DATE NULL,
  dias_trabajo VARCHAR(25) NULL,
  PRIMARY KEY (id_horarios))



-- -----------------------------------------------------
-- Table horario_empleado
-- -----------------------------------------------------
CREATE TABLE  horario_empleado (
  id_horario INT NOT NULL,
  tipo_horario VARCHAR(25) NULL,
  horarios_id_horarios INT NOT NULL,
  PRIMARY KEY (id_horario),
  CONSTRAINT fk_horario_empleado_horarios
    FOREIGN KEY (horarios_id_horarios)
    REFERENCES horarios (id_horarios)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_horario_empleado_horarios_idx ON horario_empleado (horarios_id_horarios ASC);


-- -----------------------------------------------------
-- Table cargos_empleados
-- -----------------------------------------------------
CREATE TABLE  cargos_empleados (
  id_cargo INT NOT NULL,
  nombre_cargo VARCHAR(35) NULL,
  sueldo INT NULL,
  PRIMARY KEY (id_cargo))



-- -----------------------------------------------------
-- Table Detalle Rol
-- -----------------------------------------------------
CREATE TABLE  Detalle_Rol (
  idDetalle_Rol INT NOT NULL,
  Remuneracion DECIMAL(20) NULL,
  Horas_extras INT NULL,
  Vacaciones INT NULL,
  Prestamos DECIMAL(20) NULL,
  Detalle_Rolcol VARCHAR(45) NULL,
  PRIMARY KEY (idDetalle_Rol))



-- -----------------------------------------------------
-- Table Servicios_Disponibles
-- -----------------------------------------------------
CREATE TABLE  Servicios_Disponibles (
  idServicios_Disponibles INT NOT NULL,
  Nombre_servicio VARCHAR(15) NULL,
  PRIMARY KEY (idServicios_Disponibles))



-- -----------------------------------------------------
-- Table Prestacion_servicios
-- -----------------------------------------------------
CREATE TABLE  Prestacion_servicios (
  id_horario INT NOT NULL,
  tipo_horario VARCHAR(25) NULL,
  Servicios_Disponibles_idServicios_Disponibles INT NOT NULL,
  PRIMARY KEY (id_horario),
  CONSTRAINT fk_prestacion_servicios_Servicios_Disponibles1
    FOREIGN KEY (Servicios_Disponibles_idServicios_Disponibles)
    REFERENCES Servicios_Disponibles (idServicios_Disponibles)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_prestacion_servicios_Servicios_Disponibles1_idx ON Prestacion_servicios (Servicios_Disponibles_idServicios_Disponibles ASC);


-- -----------------------------------------------------
-- Table Clientes
-- -----------------------------------------------------
CREATE TABLE  Clientes (
  idClientes INT NOT NULL,
  Nombre_cliente VARCHAR(45) NULL,
  Direccion_cliente VARCHAR(50) NULL,
  Telefono_cliente NUMERIC (10) NULL,
  Correo_cliente VARCHAR(50) NULL,
  PRIMARY KEY (idClientes))



-- -----------------------------------------------------
-- Table Agendamiento_servicios
-- -----------------------------------------------------
CREATE TABLE  Agendamiento_servicios (
  idAgendamiento_servicios INT NOT NULL,
  Fecha DATETIME NULL,
  Hora TIME NULL,
  prestacion_servicios_id_horario INT NOT NULL,
  Clientes_idClientes INT NOT NULL,
  PRIMARY KEY (idAgendamiento_servicios),
  CONSTRAINT fk_Agendamiento_servicios_prestacion_servicios1
    FOREIGN KEY (prestacion_servicios_id_horario)
    REFERENCES Prestacion_servicios (id_horario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Agendamiento_servicios_Clientes1
    FOREIGN KEY (Clientes_idClientes)
    REFERENCES Clientes (idClientes)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_Agendamiento_servicios_prestacion_servicios1_idx ON Agendamiento_servicios (prestacion_servicios_id_horario ASC);

CREATE INDEX fk_Agendamiento_servicios_Clientes1_idx ON Agendamiento_servicios (Clientes_idClientes ASC);


-- -----------------------------------------------------
-- Table Empleados
-- -----------------------------------------------------
CREATE TABLE  Empleados (
  id_empleado INT NOT NULL,
  identificacion_empleado VARCHAR(20) NULL,
  nombre_empleado VARCHAR(35) NULL,
  apellidos_empleado VARCHAR(35) NULL,
  Direccion CHAR(50) NULL,
  correo_electronico VARCHAR(45) NULL,
  Estado_civil VARCHAR(12) NULL,
  Edad INT NULL,
  pais VARCHAR(30) NULL,
  eps VARCHAR(25) NULL,
  fecha_pago DATE NULL,
  horario_trabajo VARCHAR(35) NULL,
  profesion VARCHAR(20) NULL,
  localidad_id_localidad INT NOT NULL,
  DATOS_PROFESIONALES_id_datosprofesionales INT NOT NULL,
  USUARIOS_BD_id_user INT NOT NULL,
  horario_empleado_id_horario INT NOT NULL,
  cargos_empleados_id_cargo INT NOT NULL,
  Detalle_Rol_idDetalle_Rol INT NOT NULL,
  Agendamiento_servicios_idAgendamiento_servicios INT NOT NULL,
  PRIMARY KEY (id_empleado),
  CONSTRAINT fk_EMPLEADO_localidad1
    FOREIGN KEY (localidad_id_localidad)
    REFERENCES localidad (id_localidad)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_EMPLEADO_DATOS_PROFESIONALES1
    FOREIGN KEY (DATOS_PROFESIONALES_id_datosprofesionales)
    REFERENCES Datos_profesionales (id_datosprofesionales)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_EMPLEADO_USUARIOS_BD1
    FOREIGN KEY (USUARIOS_BD_id_user)
    REFERENCES Usuarios (id_user)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_EMPLEADO_horario_empleado1
    FOREIGN KEY (horario_empleado_id_horario)
    REFERENCES horario_empleado (id_horario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_EMPLEADO_cargos_empleados1
    FOREIGN KEY (cargos_empleados_id_cargo)
    REFERENCES cargos_empleados (id_cargo)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_EMPLEADO_Detalle_Rol1
    FOREIGN KEY (Detalle_Rol_idDetalle_Rol)
    REFERENCES Detalle_Rol (idDetalle_Rol)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_EMPLEADO_Agendamiento_servicios1
    FOREIGN KEY (Agendamiento_servicios_idAgendamiento_servicios)
    REFERENCES Agendamiento_servicios (idAgendamiento_servicios)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_EMPLEADO_localidad1_idx ON Empleados (localidad_id_localidad ASC);

CREATE INDEX fk_EMPLEADO_DATOS_PROFESIONALES1_idx ON Empleados (DATOS_PROFESIONALES_id_datosprofesionales ASC);

CREATE INDEX fk_EMPLEADO_USUARIOS_BD1_idx ON Empleados (USUARIOS_BD_id_user ASC);

CREATE INDEX fk_EMPLEADO_horario_empleado1_idx ON Empleados (horario_empleado_id_horario ASC);

CREATE INDEX fk_EMPLEADO_cargos_empleados1_idx ON Empleados (cargos_empleados_id_cargo ASC);

CREATE INDEX fk_EMPLEADO_Detalle_Rol1_idx ON Empleados (Detalle_Rol_idDetalle_Rol ASC);

CREATE INDEX fk_EMPLEADO_Agendamiento_servicios1_idx ON Empleados (Agendamiento_servicios_idAgendamiento_servicios ASC);


-- -----------------------------------------------------
-- Table proveedores
-- -----------------------------------------------------
CREATE TABLE  proveedores (
  id_proveedor INT NOT NULL,
  nombre_titular VARCHAR(35) NULL,
  empresa_nombre VARCHAR(40) NULL,
  nit_empresa VARCHAR(45) NULL,
  telefono_proveedor INT NULL,
  direccion_empresa VARCHAR(45) NULL,
  sitio_web VARCHAR(45) NULL,
  ciudad VARCHAR(25) NULL,
  localidad_id_localidad INT NOT NULL,
  PRIMARY KEY (id_proveedor),
  CONSTRAINT fk_proveedores_localidad1
    FOREIGN KEY (localidad_id_localidad)
    REFERENCES localidad (id_localidad)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE INDEX fk_proveedores_localidad1_idx ON proveedores (localidad_id_localidad ASC);


-- -----------------------------------------------------
-- Table Auditoria
-- -----------------------------------------------------
CREATE TABLE  Auditoria (
  idAuditoria INT NOT NULL,
  Accion VARCHAR(200) NULL,
  Tabla VARCHAR(45) NULL,
  Fecha DATETIME NULL,
  PRIMARY KEY (idAuditoria))